package tiseddev.com.weatherclient.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.sql.SQLException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tiseddev.com.weatherclient.R;
import tiseddev.com.weatherclient.adapters.WeatherItemsAdapter;
import tiseddev.com.weatherclient.databinding.ActivityMainBinding;
import tiseddev.com.weatherclient.interfaces.ReceiveNewInfoListener;
import tiseddev.com.weatherclient.models.WeatherAnswer;
import tiseddev.com.weatherclient.services.BGService;
import tiseddev.com.weatherclient.utils.Const;
import tiseddev.com.weatherclient.utils.DBUtils.HelperFactory;
import tiseddev.com.weatherclient.utils.WeatherApiClient;


public class MainActivity extends AppCompatActivity implements ReceiveNewInfoListener {

    private static final String TAG = "MAIN ACTIVITY";

    @BindView(R.id.weather_recycler)
    RecyclerView weatherRecycler;

    @BindView(R.id.coordinator)
    CoordinatorLayout coordinatorLayout;

    WeatherItemsAdapter adapter;
    Snackbar snackbar;
    WeatherApiClient weatherApiClient;

    BroadcastReceiver receiver;
    BroadcastReceiver progressReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        ButterKnife.bind(this);

        binding.setCity(Const.predefinedCity);

        adapter = new WeatherItemsAdapter(this);

        weatherApiClient = WeatherApiClient.init();

        weatherRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        weatherRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        weatherRecycler.setAdapter(adapter);

        initWeatherFromDB();

        startService(new Intent(this, BGService.class));

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                WeatherAnswer weatherAnswer = (WeatherAnswer) intent.getSerializableExtra("weatherInfo");

                newInfoReceived(weatherAnswer);
            }
        };

        progressReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String type = intent.getStringExtra("progressType");

                switch (type) {

                    case "loading":
                        loadingStarted();
                        break;

                    case "loaded":
                        loadingFinished();
                        break;

                    case "error":
                        loadingError();
                        break;

                    default:
                        loadingFinished();
                        break;
                }
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(Const.NEW_INFO_INTENT));
        LocalBroadcastManager.getInstance(this).registerReceiver((progressReceiver),
                new IntentFilter(Const.PROGRESS_INTENT));
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(progressReceiver);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initWeatherFromDB() {

        try {
            List<WeatherAnswer> weatherAnswers = HelperFactory.getHelper().getWeatherAnswerDAO().getAllWeatherInfo();

            adapter.addItems(weatherAnswers);
        } catch (SQLException e) {
            Log.e(TAG, "error while trying to read DB " + Log.getStackTraceString(e));
        }
    }

    @Override
    public void newInfoReceived(WeatherAnswer weatherAnswer) {

        Log.d(TAG, "received in activity");
        adapter.addItem(weatherAnswer);
    }

    @Override
    public void loadingStarted() {
       snackbar = Snackbar.make(coordinatorLayout, "Loading new info", Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

    @Override
    public void loadingFinished() {

        snackbar.setText("Done");
        snackbar.setDuration(Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void loadingError() {

        snackbar.setText("Error");
        snackbar.setDuration(Snackbar.LENGTH_SHORT);
        snackbar.show();
    }
}
