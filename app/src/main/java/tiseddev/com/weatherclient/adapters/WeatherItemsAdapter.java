package tiseddev.com.weatherclient.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tiseddev.com.weatherclient.R;
import tiseddev.com.weatherclient.databinding.ItemWeatherInfoBinding;
import tiseddev.com.weatherclient.interfaces.CRUDAdapterInterface;
import tiseddev.com.weatherclient.models.WeatherAnswer;


public class WeatherItemsAdapter extends RecyclerView.Adapter<WeatherItemsAdapter.ViewHolder> implements CRUDAdapterInterface<WeatherAnswer> {

    Context context;
    LayoutInflater layoutInflater;
    List<WeatherAnswer> weatherAnswerList;

    public WeatherItemsAdapter(Context context) {

        this.context = context;
        weatherAnswerList = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ItemWeatherInfoBinding itemWeatherInfoBinding;

        public ViewHolder(ItemWeatherInfoBinding itemWeatherInfoBinding) {
            super(itemWeatherInfoBinding.getRoot());

            this.itemWeatherInfoBinding = itemWeatherInfoBinding;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ItemWeatherInfoBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_weather_info,
                parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        WeatherAnswer currentWeatherAnswer = getItem(position);

        holder.itemWeatherInfoBinding.setWeatherItem(currentWeatherAnswer);
    }

    @Override
    public int getItemCount() {
        return weatherAnswerList.size();
    }

    @Override
    public WeatherAnswer getItem(int position) {

       return weatherAnswerList.get(position);
    }

    @Override
    public void addItem(WeatherAnswer item) {

        weatherAnswerList.add(item);
        Collections.sort(weatherAnswerList);
        notifyDataSetChanged();
    }

    @Override
    public void removeItem(WeatherAnswer item) {

        weatherAnswerList.remove(item);
        Collections.sort(weatherAnswerList);
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<WeatherAnswer> items) {

        weatherAnswerList.addAll(items);
        Collections.sort(weatherAnswerList);
        notifyDataSetChanged();
    }

    @Override
    public void clearList() {

        weatherAnswerList.clear();
        notifyDataSetChanged();
    }

    @Override
    public void updateItem(WeatherAnswer oldItem, WeatherAnswer item) {

        weatherAnswerList.set(weatherAnswerList.indexOf(oldItem), item);
        notifyDataSetChanged();
    }

    @Override
    public void showItem(WeatherAnswer item) {

    }

    @Override
    public List<WeatherAnswer> getItems() {
        return weatherAnswerList;
    }
}
