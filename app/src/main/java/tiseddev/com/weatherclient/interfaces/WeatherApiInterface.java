package tiseddev.com.weatherclient.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import tiseddev.com.weatherclient.models.WeatherAnswer;

public interface WeatherApiInterface {

    @GET("/data/2.5/weather")
    Call<WeatherAnswer> getWeatherInfo(@Query("q") String city, @Query("units") String units,
                                       @Query("appid") String apiKey);
}
