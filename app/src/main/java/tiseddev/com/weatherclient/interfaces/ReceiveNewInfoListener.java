package tiseddev.com.weatherclient.interfaces;

import tiseddev.com.weatherclient.models.WeatherAnswer;


public interface ReceiveNewInfoListener {

    void newInfoReceived(WeatherAnswer weatherAnswer);
    void loadingStarted();
    void loadingFinished();
    void loadingError();
}