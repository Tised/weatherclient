package tiseddev.com.weatherclient.models.weather_models;

import com.google.gson.annotations.SerializedName;

public class Clouds {

    public String getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = all;
    }

    @SerializedName("all")
    String all;

    @Override
    public String toString() {
        return "Clouds{" +
                "all='" + all + '\'' +
                '}';
    }
}
