package tiseddev.com.weatherclient.models.weather_models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Coord {

    @DatabaseField(generatedId=true)
    public int id;

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @SerializedName("lon")
    @DatabaseField
    double lon;

    @SerializedName("lat")
    @DatabaseField
    double lat;
}
