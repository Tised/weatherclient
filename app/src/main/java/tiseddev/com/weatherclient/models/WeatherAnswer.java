package tiseddev.com.weatherclient.models;


import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.List;

import tiseddev.com.weatherclient.models.weather_models.Clouds;
import tiseddev.com.weatherclient.models.weather_models.Coord;
import tiseddev.com.weatherclient.models.weather_models.Main;
import tiseddev.com.weatherclient.models.weather_models.Sys;
import tiseddev.com.weatherclient.models.weather_models.Weather;
import tiseddev.com.weatherclient.models.weather_models.Wind;

@DatabaseTable
public class WeatherAnswer implements Serializable, Comparable<WeatherAnswer> {

    @DatabaseField(generatedId = true, columnName = "id")
    int id;

    @SerializedName("coord")
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    Coord coord;

    @SerializedName("weather")
    List<Weather> weatherList;

    @SerializedName("base")
    String base;

    @SerializedName("main")
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    Main main;

    @SerializedName("wind")
    Wind wind;

    @SerializedName("clouds")
    Clouds clouds;

    @SerializedName("dt")
    long dt;

    @SerializedName("sys")
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    Sys sys;

    @SerializedName("idAnswer")
    String idAnswer;

    @SerializedName("name")
    String name;

    @SerializedName("cod")
    String cod;

    @DatabaseField
    long loadDate;

    public long getLoadDate() {
        return loadDate;
    }

    public void setLoadDate(long loadDate) {
        this.loadDate = loadDate;
    }

    public WeatherAnswer() {}

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdAnswer() {
        return idAnswer;
    }

    public void setIdAnswer(String idAnswer) {
        this.idAnswer = idAnswer;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public List<Weather> getWeatherList() {
        return weatherList;
    }

    public void setWeatherList(List<Weather> weatherList) {
        this.weatherList = weatherList;
    }

    @Override
    public String toString() {
        return "WeatherAnswer{" +
                "id=" + id +
                ", coord=" + coord +
                ", weatherList=" + weatherList +
                ", base='" + base + '\'' +
                ", main=" + main +
                ", wind=" + wind +
                ", clouds=" + clouds +
                ", dt=" + dt +
                ", sys=" + sys +
                ", idAnswer='" + idAnswer + '\'' +
                ", name='" + name + '\'' +
                ", cod='" + cod + '\'' +
                '}';
    }

    @Override
    public int compareTo(WeatherAnswer another) {

        if (another.getLoadDate() > loadDate) {

            return 1;
        }

        if (another.getLoadDate() < loadDate) {

            return -1;
        } else {

            return 0;
        }
    }
}
