package tiseddev.com.weatherclient.models.weather_models;

import com.google.gson.annotations.SerializedName;

public class Wind {

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getDeg() {
        return deg;
    }

    public void setDeg(double deg) {
        this.deg = deg;
    }

    @SerializedName("speed")
    double speed;

    @SerializedName("deg")
    double deg;

    @Override
    public String toString() {
        return "Wind{" +
                "speed=" + speed +
                ", deg=" + deg +
                '}';
    }
}
