package tiseddev.com.weatherclient.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import tiseddev.com.weatherclient.models.WeatherAnswer;


public class WeatherAnswerDAO extends BaseDaoImpl<WeatherAnswer, String> {

    public WeatherAnswerDAO(ConnectionSource connectionSource,
                            Class<WeatherAnswer> weatherAnswerClass) throws SQLException {
        super(connectionSource, weatherAnswerClass);

    }

    public List<WeatherAnswer> getAllWeatherInfo() throws SQLException {

        return this.queryForAll();
    }
}
