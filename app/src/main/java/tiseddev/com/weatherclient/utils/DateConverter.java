package tiseddev.com.weatherclient.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateConverter {

    public static String convert(long timestamp) {
        Calendar mydate = Calendar.getInstance();
        mydate.setTimeInMillis(timestamp * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        return sdf.format(mydate.getTime());
    }
}
