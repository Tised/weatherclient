package tiseddev.com.weatherclient.utils.DBUtils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import tiseddev.com.weatherclient.dao.WeatherAnswerDAO;
import tiseddev.com.weatherclient.models.WeatherAnswer;
import tiseddev.com.weatherclient.models.weather_models.Coord;
import tiseddev.com.weatherclient.models.weather_models.Main;
import tiseddev.com.weatherclient.models.weather_models.Sys;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    public static String TAG = DatabaseHelper.class.getName();

    private static String DATABASE_NAME ="weather_client.db";

    private static final int DATABASE_VERSION = 1;

    private WeatherAnswerDAO weatherAnswerDAO = null;

    Context context;

    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource){
        try {

            TableUtils.createTableIfNotExists(connectionSource, WeatherAnswer.class);
            TableUtils.createTableIfNotExists(connectionSource, Coord.class);
            TableUtils.createTableIfNotExists(connectionSource, Main.class);
            TableUtils.createTableIfNotExists(connectionSource, Sys.class);
        }

        catch (SQLException e){

            Log.e(TAG, "error creating DB " + DATABASE_NAME);

            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer){

        switch (oldVer) {
            case 1:
                updateFromVersion1(db, connectionSource, oldVer, newVer);
                break;

            default:
                // no updates needed
                break;
        }
    }

    @Override
    public void close(){
        super.close();
        weatherAnswerDAO = null;
    }

    private void updateFromVersion1(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer, int newVer) {

        //some update here
        onUpgrade(db, connectionSource, oldVer + 1, newVer);
    }

    public WeatherAnswerDAO getWeatherAnswerDAO() throws SQLException {

        if(weatherAnswerDAO == null){
            weatherAnswerDAO = new WeatherAnswerDAO(getConnectionSource(), WeatherAnswer.class);
        }

        return weatherAnswerDAO;
    }
}
