package tiseddev.com.weatherclient.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class BGService extends Service {

    private static final String TAG = "SERVICE";
    long TIME_PERIOD = 1000 * 60 * 10; // 10 min

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "service started");

        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                startService(new Intent(BGService.this, DownloadService.class));
            }
        }, 1, TIME_PERIOD);

        return Service.START_STICKY_COMPATIBILITY;
    }
}
