package tiseddev.com.weatherclient.receivers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import tiseddev.com.weatherclient.services.BGService;

public class BootReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = "BOOT";

    @Override
    public void onReceive(Context context, Intent intent) {

        context.startService(new Intent(context, BGService.class));
    }
}

